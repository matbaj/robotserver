#!/usr/bin/env python2
from socket import *
import socket
import code
import cmd
import os
import serial
import time
import math
import threading
import select
import sys

dl = 14.3*math.pi
rozstawl= 29*math.pi

def scale(x,in_min, in_max, out_min, out_max):
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

def array_to_string(array):
  def join(t):
    return "%s:%s" % t
  return '|'.join(map(join,array))


class RobotInterface:
  def __init__(self,port,speed):
    print "Connecting to Bimo!"
    self.serial = serial.Serial(port,speed,timeout=2) 
    if self.serial.isOpen():
      print "Connected to BIMO!"
    else:
      print "AAAAA.... Port isn't open ... i i i i... I DON'T KNOW WHAT TO DO NOW"



  def __del__(self):
    self.serial.close()

  def clear_serial(self):
    something_to_say = True
    while something_to_say:
      text = self.serial.read()
      print("Bimo mowi: %s" % text)
      if text == '':
        something_to_say = False  

  def set_head(self,angle):
    val  = scale(angle,0, 180, 0, 175)
    print "Ustawiam : %s " % val
    self.serial.write("H%dZ" % val)

  def read_sensors(self):
    self.serial.write("DZ")
    return self.serial.readline()

  def move(self, dist):
    self.serial.write("M%dZ" % dist)
    something_to_say = True
    while something_to_say:
      text = self.serial.read()
      print("Bimo mowi: |%s|" % text)
      if text == '9':
       something_to_say = False

  def move_forward(self,dist):
    command = int(61440+(dist*200/dl))
    self.move(command)
    return "DONE"

  def move_backward(self,dist):
    command = int(40960+(dist*200/dl))
    self.move(command)
    return "DONE"

  def rotate_lefta(self,dist):
    dist = 45056+dist
    self.move(dist)

  def rotate_righta(self,dist):
    dist = 57344+dist
    self.move(dist)

  def rotate_left(self,rad):
    dist = (rad*rozstawl/360)*200/dl
    dist = 45056+dist
    self.move(dist)
    return "DONE"

  def rotate_right(self,rad):
    dist = (rad*rozstawl/360)*200/dl
    dist = 57344+dist
    self.move(dist)
    return "DONE"

  def rotation(self,rad):
    if rad > 0:
      self.rotate_right( rad )
    else:
      self.rotate_left(rad*(-1))
    return "DONE"

  def look(self):
    angle = 0
    m_array_a = []
    m_array_b = []
    while (angle <=180):
      self.set_head(angle)
      data = self.read_sensors()
      if data == '':
        data = self.read_sensors()
      array = data.split("|")
      if int(array[0]) != 0:
        m_a = int(array[0])
        print "A: %s" % m_a
        m_array_a.append((angle,m_a))
      if int(array[1]) != 0:
        m_b = int(array[1])
        print "B: %s" % m_b
        m_array_b.append((angle+180,m_b))

      
      angle += 1
      response = array_to_string(m_array_a+m_array_b)
    return response

class ServerTCP(threading.Thread): 
  def __init__(self, port=8888): 
    threading.Thread.__init__(self)
    self._stop = threading.Event()
    self.host = ''
    self.pipe = 0
    self.port = port 
    self.backlog = 5 
    self.size = 10240 
    self.server = None 
    self.threads = [] 
    self.pipe = os.pipe()

    
  def open_socket(self): 
      try: 
          self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
          self.server.bind((self.host,self.port)) 
          self.server.listen(5) 
      except socket.error, (value,message): 
          if self.server: 
              self.server.close() 
          print "Could not open socket: " + message 
          sys.exit(1) 
  
  def run(self): 
    self.open_socket() 
    input = [self.server,self.pipe[0]] 
    self.running = 1 
    while self.running: 
      try:
        inputready,outputready,exceptready = select.select(input,[],[]) 
      except:
        print 'Something is wrong'
        print sys.exc_info()
        inputready = ''
        self.running = 0

      for s in inputready: 
        if s == self.server: 
          # handle the server socket 
          c = Client(self.server.accept()) 
          c.start() 
          self.threads.append(c)
        elif s == self.pipe[0]: 
          try:
            cmd  = os.read(self.pipe[0], 64).split(":")
          except:
            cmd = ['servertcp', 'quit']
          if cmd[0] == 'servertcp':
            if cmd[1] =='quit':
              print 'ServerTCP: shutting down'
              self.running = 0

    #disconnect all clients
    for node in self.threads:
      node.quit()
    self.server.close() 
    # close all threads 
    for c in self.threads: 
      c.join() 
 
class Client(threading.Thread): 
  def __init__(self,(client,address)): 
    threading.Thread.__init__(self) 
    self.client = client 
    self.address = address 
    self.size = 1024 
    self.pipe = os.pipe()

  def quit(self):
    os.write(self.pipe[1], 'node:quit')

  def run(self):
    self.robot = RobotInterface('/dev/rfcomm0',115200)
    print "New Client", self.address  
    running = 1
    self.authorized = False 
    self.count_try = 0
    input = [self.client,self.pipe[0]]
    while running:
      try:
        inputready,outputready,exceptready = select.select(input,[],[]) 
      except:
        print 'Something is wrong'
        running = 0
      for s in inputready: 
        if s == self.client:
            msg = s.recv(self.size)
            if msg:
              print "MSG: %s" % msg
              state, val = msg.split(':')

              response = ''

              if state == 'FORWARD':
                  response = self.robot.move_forward( int(val) )
              elif state == 'BACKWARD':
                  response = self.robot.move_backward( int(val) )
              elif state == 'ROTATION':
                  response = self.robot.rotation( int(val) )
              elif state == 'LOOK':
                  response = self.robot.look()
              else:
                  response = ''

              s.send(response)
            else:
              running = 0
        elif s == self.pipe[0]: 
          try:
            cmd  = os.read(self.pipe[0], 64).split(":")
          except:
            cmd = ['node', 'quit']
          if cmd[0] == 'node':
            if cmd[1] =='quit':
              running = 0 
    print "Closing connection with ", self.address       
    self.client.close()  

class Server(cmd.Cmd):
  prompt = "(Server)>"

  buf_size = 10240

  def do_inter(self, line):
    """Droping to interpreter"""
    code.interact(local=locals())

  def do_client_list(self,line):
    """client_list - list of connected clients"""
    for client in server.threads:
      print client.address 

  def do_quit(self, line):
    """Quit from client"""
    os.write(server.pipe[1], 'servertcp:quit')
    return True

  def do_EOF(self, line):
    """Quit from client"""
    os.write(server.pipe[1], 'servertcp:quit')
    return True


if __name__ == '__main__':
    print "Robot Server"
    cli = Server()
    server = ServerTCP()

    server.start()
    try:
        cli.cmdloop()
    except KeyboardInterrupt:
        cli.do_quit('')
